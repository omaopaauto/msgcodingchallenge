from utils import (
    starting_city,
    add_edge_to_tour
)

def get_degree_of_city(tour_graph, city):
  if tour_graph.has_node(city):
    return tour_graph.degree(city)
  else:
    return 0

# As we construct a circle the last last city added is always the only city except the starting city with degree 1
def get_last_city(tour_graph):
  for city in tour_graph:
    if tour_graph.degree(city) == 1 and not city == starting_city:
      return city

# Compute a valid tour on the graph by recursively going through the minimum spanning tree, starting with the
# starting city
def compute_tour(complete_graph, min_spanning_tree, current_city, tour_graph):
  for next_city in min_spanning_tree.neighbors(current_city):
    if not tour_graph.has_edge(current_city, next_city):
      next_city_degree = get_degree_of_city(tour_graph, next_city)
      current_city_degree = get_degree_of_city(tour_graph, current_city)
      last_city = get_last_city(tour_graph)
      # If we come back to the starting city and already added other cities, continue computation with next city
      # as we want to end in the starting city and therefore add no further edges to it.
      if current_city == starting_city and current_city_degree > 0:
        compute_tour(complete_graph, min_spanning_tree, next_city, tour_graph)
      # If the next city is the starting city, but there is still another city with an open end, connect to the 
      # last city to continue the circle
      elif next_city == starting_city and not last_city is None and not last_city == current_city:
        add_edge_to_tour(complete_graph, tour_graph, current_city, last_city)
      # If the next city and the current city are still available, connect both and continue computation with the
      # next city
      elif next_city_degree < 2 and current_city_degree < 2:
        add_edge_to_tour(complete_graph, tour_graph, current_city, next_city)
        compute_tour(complete_graph, min_spanning_tree, next_city,tour_graph)
      # If current city already has two neighbors, it is already in the circle. In order not to break it, connect
      # the next city to the last city to continur the circle
      elif next_city_degree < 2 and current_city_degree == 2:
        if not last_city is None:
          add_edge_to_tour(complete_graph, tour_graph, next_city, last_city)
          compute_tour(complete_graph, min_spanning_tree, next_city, tour_graph)
  # Close the circle by connecting the last city with the starting city
  if current_city == starting_city:
    last_city = get_last_city(tour_graph)
    add_edge_to_tour(complete_graph, tour_graph, current_city, last_city)
