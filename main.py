import pandas
import networkx as nx

import distance_calculation as dc
import tour_computation as tc
import postprocessing as pp

from utils import (
  col_city_name,
  col_latitude,
  col_longitude,
  starting_city,
  weight_attr
)

# Computes the distances between every city and creates a graoh with the distance as the edges' weight
def initialize_complete_graph(data):
  complete_graph = nx.Graph()
  for i, city in data.iterrows():
    complete_graph.add_node(city[col_city_name])
  for i, city1 in data.iterrows():
    for j, city2 in data.iterrows():
      if not i == j:
        distance = dc.calculate_distance_in_km(city1[col_latitude], city1[col_longitude], city2[col_latitude], city2[col_longitude])
        complete_graph.add_edge(city1[col_city_name], city2[col_city_name], weight=distance)
  return complete_graph

def print_path(tour_graph):
  paths = nx.all_simple_paths(tour_graph, source=starting_city, target=list(tour_graph.neighbors(starting_city))[0])
  for path in paths:
    if len(path) > 2:
      print('The optimal path is:')
      print(path + [starting_city])

# In order to get a good tour we first compute a minimum spanning tree to get the least expensive connections
# between the cities. Based on the tree we compute a first valid tour. As this tour might contain some poor decisions
# regarding the overall distance, we later check that every city is connected to its closest neighbors or switch 
# the tour accordingly. In the end we check if the tour gets better when switching two succeeding cities along the 
# tour. The result should then be a good enough tour (not necessarily optimal as this is computationally expensive)
data = pandas.read_csv('msg_standorte_deutschland.csv')
complete_graph = initialize_complete_graph(data)
min_spanning_tree = nx.minimum_spanning_tree(complete_graph)
tour_graph = nx.Graph()
tc.compute_tour(complete_graph, min_spanning_tree, starting_city, tour_graph)
pp.check_for_city_with_no_edge_to_closest_neighbors(complete_graph, tour_graph)
pp.switch_cities(complete_graph, tour_graph)
print_path(tour_graph)
print('Total length of the tour:', tour_graph.size(weight_attr), 'km')

