from utils import (
  add_edge_to_tour,
  weight_attr
)

def get_two_closest_cities(complete_graph, city_name):
  return sorted(complete_graph[city_name].items(), key=lambda e: e[1][weight_attr])[:2]

# Check every city on the tour after computing a valid tour whether they are connected to their closest neighbors.
# Note that we only want to catch those cities which might have been left out by the computation ending up between 
# two far away cities
def check_for_city_with_no_edge_to_closest_neighbors(complete_graph, tour_graph):
  edges_to_be_removed = list()
  for city in complete_graph:
    closest_cities = get_two_closest_cities(complete_graph, city)
    closest_city1 = closest_cities[0][0]
    closest_city2 = closest_cities[1][0]
    if not (tour_graph.has_edge(city, closest_city1) or tour_graph.has_edge(city, closest_city2)):
      # Only change something if the two closest neighbors are connected to each other on the tour
      # Otherwise rewiring the edges to the closest neighbors could have bad impacts on the tour elsewhere
      if closest_city1 in tour_graph.neighbors(closest_city2):
        old_neighbors = [neighbor for neighbor in tour_graph.neighbors(city)]
        add_edge_to_tour(complete_graph, tour_graph, old_neighbors[0], old_neighbors[1])
        for neighbor in old_neighbors:
          edges_to_be_removed.append((neighbor, city))
        add_edge_to_tour(complete_graph, tour_graph, city, closest_city1)
        add_edge_to_tour(complete_graph, tour_graph, city, closest_city2)
        edges_to_be_removed.append((closest_city1, closest_city2))
  tour_graph.remove_edges_from(edges_to_be_removed)

# Compare the dustabces before and after a possible change by comparing the edges that would be removed with the edges
# that would be added in the case of switching
def is_switch_an_improvement(complete_graph, city1, city2, neighbor_of_city1, neighbor_of_city2):
  distance_to_neighbors_before_switch = complete_graph[city1][neighbor_of_city1][weight_attr] + complete_graph[city2][neighbor_of_city2][weight_attr]
  distance_to_neighbors_after_switch = complete_graph[city1][neighbor_of_city2][weight_attr] + complete_graph[city2][neighbor_of_city1][weight_attr]
  return distance_to_neighbors_before_switch > distance_to_neighbors_after_switch

# Checks every two succeeding cities if the distance decreases when switching the order of them in the path
# and switches them, if that is the case
def switch_cities(complete_graph, tour_graph):
  for edge in tour_graph.edges:
    city1 = edge[0]
    city2 = edge[1]
    neighbors_of_city1 = list(tour_graph.neighbors(city1))
    neighbors_of_city2 = list(tour_graph.neighbors(city2))
    neighbor_of_city1 = neighbors_of_city1[0] if neighbors_of_city1[1] == city2 else neighbors_of_city1[1]
    neighbor_of_city2 = neighbors_of_city2[0] if neighbors_of_city2[1] == city1 else neighbors_of_city2[1]
    if is_switch_an_improvement(complete_graph, city1, city2, neighbor_of_city1, neighbor_of_city2):
      add_edge_to_tour(complete_graph, tour_graph, city1, neighbor_of_city2)
      add_edge_to_tour(complete_graph, tour_graph, city2, neighbor_of_city1)
      tour_graph.remove_edge(city1, neighbor_of_city1)
      tour_graph.remove_edge(city2, neighbor_of_city2)
