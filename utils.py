#Constants
earth_radius = 6371
col_city_name = 'msg Standort'
col_latitude = 'Breitengrad'
col_longitude = 'Längengrad'
starting_city = 'Ismaning/München (Hauptsitz)'
weight_attr = 'weight'


def add_edge_to_tour(complete_graph,tour_graph,city_name1,city_name2):
    tour_graph.add_edge(city_name1, city_name2, weight=complete_graph[city_name1][city_name2][weight_attr])
