import math
from utils import earth_radius

#Source: https://stackoverflow.com/questions/365826/calculate-distance-between-2-gps-coordinates
def degrees_to_radians(degrees):
  return degrees * math.pi / 180
  
def calculate_distance_in_km(lat1,lon1,lat2,lon2):
  rad_diff_lat = degrees_to_radians(lat2-lat1)
  rad_diff_lon = degrees_to_radians(lon2-lon1)
  
  rad_lat1 = degrees_to_radians(lat1)
  rad_lat2 = degrees_to_radians(lat2)
  
  sin_lat_diff = math.sin(rad_diff_lat/2) * math.sin(rad_diff_lat/2)
  sin_lon_diff = math.sin(rad_diff_lon/2) * math.sin(rad_diff_lon/2)
  a = sin_lat_diff + sin_lon_diff * math.cos(rad_lat1) * math.cos(rad_lat2)
  c = 2 * math.atan2(math.sqrt(a),math.sqrt(1-a))
  return c * earth_radius
